package com.space.service;

import com.space.model.Ship;
import com.space.repository.SpaceRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class SpaceServiceImpl implements SpaceService {

    private final SpaceRepository spaceRepository;


    @Override
    public Ship createShip() {
        return null;
    }

    @Override
    public List<Ship> getShips() {
        return null;
    }

    @Override
    public Integer getShipsCount() {
        return getShips().size();
    }
}
