package com.space.service;

import com.space.model.Ship;

import java.util.List;

public interface SpaceService {

    Ship createShip();

    List<Ship> getShips();

    Integer getShipsCount();
}
