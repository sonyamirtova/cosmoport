package com.space.repository;

import com.space.controller.ShipOrder;
import com.space.model.Ship;
import com.space.model.ShipType;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface SpaceRepository extends JpaRepository<Ship, Long> {

    List<Ship> findAllByNameLike(String name);

    List<Ship> findAllByPlanetLike(String planet);

    List<Ship> findAllByShipType(ShipType shipType);

    List<Ship> findAllByProdDateAfter(Long date);

    List<Ship> findAllByProdDateBefore(Long date);

    List<Ship> findAllByIsUsed(Boolean bool);

    List<Ship> findAllBySpeedGreaterThan(Double speed);

    List<Ship> findAllBySpeedLessThan(Double speed);

    List<Ship> findAllByCrewSizeGreaterThanEqual(Integer crewSize);

    List<Ship> findAllByCrewSizeLessThanEqual(Integer crewSize);

//    List<Ship> findAllByShipOrder(ShipOrder shipOrder);

    List<Ship> findAllByShipOrder(String shipOrder);

    //pageNumber; pageSize;




}
