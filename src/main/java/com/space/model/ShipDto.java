package com.space.model;

import lombok.Data;
import org.springframework.lang.NonNull;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.*;

@Data
public class ShipDto {

    /*
    “name”:[String],
“planet”:[String],
“shipType”:[ShipType],
“prodDate”:[Long],
“isUsed”:[Boolean], --optional, default=false
“speed”:[Double],
“crewSize”:[Integer]
     */

    @NotBlank
    @Size(max = 50)
    private String name;

    @NotBlank
    @Size(max = 50)
    private String planet;

    private ShipType shipType;

    @PositiveOrZero
    private Long prodDate;

    private Boolean isUsed;

    private Double speed;

    private Integer crewSize;


}
