package com.space.controller;

import com.space.model.Ship;
import com.space.model.ShipDto;
import com.space.model.ShipType;
import com.space.service.SpaceService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@org.springframework.web.bind.annotation.RestController
@RequestMapping("/rest")
@RequiredArgsConstructor
public class SpaceController {

    private final SpaceService spaceService;

    @GetMapping("/ships")
    public ResponseEntity<List<Ship>> getShips(@RequestParam()String name,
                                               @RequestParam()String planet,
                                               @RequestParam()ShipType shipType,
                                               @RequestParam()Long after,
                                               @RequestParam()Long before,
                                               @RequestParam()Double minSpeed,
                                               @RequestParam()Double maxSpeed,
                                               @RequestParam()Integer minCrewSize,
                                               @RequestParam()Integer maxCrewSize,
                                               @RequestParam()Double minRating,
                                               @RequestParam()Double maxRating) {
        return null;
    }


    @GetMapping("/ships/count")
    public Integer getShipsCount(@RequestParam()String name,
                            @RequestParam()String planet,
                            @RequestParam()ShipType shipType,
                            @RequestParam()Long after,
                            @RequestParam()Long before,
                            @RequestParam()Double minSpeed,
                            @RequestParam()Double maxSpeed,
                            @RequestParam()Integer minCrewSize,
                            @RequestParam()Integer maxCrewSize,
                            @RequestParam()Double minRating,
                            @RequestParam()Double maxRating) {
        return spaceService.getShipsCount();
    }

    @PostMapping("/ships")
    public ResponseEntity<Ship> createShip(@RequestBody @Validated ShipDto shipDto) {
        Ship ship = spaceService.createShip();
        return ResponseEntity.ok(ship);
    }

    @GetMapping("/ships/")
    public ResponseEntity<Ship> getShip() {

    }

}
